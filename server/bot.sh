#!/bin/bash
cd /home/site/private/botfw/
IDUNIQ=$(date +%s)$(shuf -i 0-99999 -n1)
python3 -m deemix -l $1 | tee $IDUNIQ && funkwhale-cli upload -l bot $(tail -n1 $IDUNIQ) && echo $1 >> /home/site/private/botfw/history.log || echo $1 >> /home/site/private/botfw/error.log
rm $(tail -n1 $IDUNIQ) -r
rm $IDUNIQ
