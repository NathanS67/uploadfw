<?php include_once('../header.html'); ?>
<main>
    <form action="." method="post">
        <label for="url">URL Deezer (Artiste/Album/Titre) : </label>
        <input type="text" id="url" name="url"><br><br>
        <input type="submit" value="Envoyer">
    </form>
    <?php
    $path = "botfw";
    $url = array();
    exec("cat $path/history.log", $url);
    $lienstest = array();
    exec("ps -eo cmd | grep \"^/bin/bash $path/bot.sh\" | cut -d' ' -f3", $lienstest);
    if ($_POST["url"]) {
        if (substr($_POST["url"], 0, 23) == "https://www.deezer.com/") {
            if (!in_array($_POST["url"], $url) && !in_array($_POST["url"], $lienstest)) {
                shell_exec("$path/bot.sh " . $_POST["url"] . " 2>&1 | tee -a $path/bot.log 2>/dev/null >/dev/null &");
                echo "<p> Téléchargement de : <a href=" . $_POST["url"] . ">" . $_POST["url"] . "</a></p>";
            } else {
                echo "<p>ERREUR : Contenu du lien déjà téléchargé.</p>";
            }
        } else {
            echo "<p>ERREUR : URL non valide.</p>";
        }
    }
    ?>
    <p>Liens en cours de traitement : </p>
    <ul>
        <?php
        $liens = array();
        exec("ps -eo cmd | grep \"^/bin/bash $path/bot.sh\" | cut -d' ' -f3", $liens);
        foreach ($liens as $album) {
            echo "<li><a href=" . $album . ">" . $album . "</a></li>";
        }
        ?>
    </ul>
    <p>En cours de traitement : </p>
    <ul>
        <?php
        $traitement = array();
        exec("find $path/ -mindepth 2 -maxdepth 2 -type d -exec basename {} \;", $traitement);
        foreach ($traitement as $album) {
            echo "<li>" . $album . "</li>";
        }
        ?>
    </ul>
    <p>Historique de téléchargements : </p>
    <ul>
        <?php
        $historique = array();
        exec("cat $path/history.log", $historique);
        foreach ($historique as $album) {
            echo "<li><a href=" . $album . ">" . $album . "</a></li>";
        }
        ?>
    </ul>
</main>
<?php include_once('../footer.html'); ?>